<?php namespace Site\Topo\Models;

use Model;

/**
 * Team Model
 */
class Team extends Model
{

    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'site_topo_teams';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'content_image' => ['System\Models\File'],
    ];

    public $attachMany = [];

    public $rules = [
        'name'           => 'required|max:255',
        'role'           => 'required|max:255',
        'content_image'  => 'required',
        'facebook'       => 'url',
        'twitter'        => 'url',
        'email'          => 'required|email',
        'description'    => 'required'
    ];
}