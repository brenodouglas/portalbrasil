<?php namespace Site\Topo\Models;

use Model;

/**
 * Portifolio Model
 */
class Portifolio extends Model
{

    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'site_topo_portifolios';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];

    public $hasMany = [];

    public $belongsTo = [
        'portifolio_category' => ['Site\Topo\Models\PortifolioCategory']
    ];

    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [
        'images' => ['System\Models\File']
    ];

    public $rules = [
        'images'       => 'required',
        'name'         => 'required|max:255',
        'description'  => 'required',
        'site'         => 'url|max:255',
        'cliente'      => 'required|max:255',
        'date_project' => 'required'
    ];

}