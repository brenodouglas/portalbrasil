<?php namespace Site\Topo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreatePortifolioCategoriesTable extends Migration
{

    public function up()
    {
        Schema::create('site_topo_portifolio_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 255);
            $table->timestamps();
            
        });
    }

    public function down()
    {
        Schema::dropIfExists('site_topo_portifolio_categories');
    }

}
