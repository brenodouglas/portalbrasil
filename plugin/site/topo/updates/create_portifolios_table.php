<?php namespace Site\Topo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreatePortifoliosTable extends Migration
{

    public function up()
    {
        Schema::create('site_topo_portifolios', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('portifolio_category_id')->unsigned()->index();
            $table->string('name', 255);
            $table->text('description');
            $table->string('cliente');
            $table->date('date_project');
            $table->string('site')->nullable();
            $table->timestamps();

            $table->foreign('portifolio_category_id')->references('id')->on('site_topo_portifolio_categories');
        });
    }

    public function down()
    {
        Schema::dropIfExists('site_topo_portifolios');
    }

}
