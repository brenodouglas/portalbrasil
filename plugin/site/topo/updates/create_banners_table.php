<?php namespace Site\Topo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateBannersTable extends Migration
{

    public function up()
    {
        Schema::create('site_topo_banners', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 200);
            $table->text('description');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('site_topo_banners');
    }

}
