<?php namespace Site\Topo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateSkillsTable extends Migration
{

    public function up()
    {
        Schema::create('site_topo_skills', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->integer('value');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('site_topo_skills');
    }

}
