<?php namespace Site\Topo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ModifyTeamsTable extends Migration
{

    public function up()
    {
        Schema::table('site_topo_teams', function($table)
        {
            $table->dropColumn('image');
        });
    }

    public function down()
    {
        Schema::table('site_topo_teams', function($table)
        {
            $table->string('image');
        });
    }

}
