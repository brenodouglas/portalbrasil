<?php namespace Site\Topo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateTeamsTable extends Migration
{

    public function up()
    {
        Schema::create('site_topo_teams', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 255);
            $table->string('role', 255);
            $table->text('description');
            $table->string('facebook', 255)->nullable();
            $table->string('twitter', 255)->nullable();
            $table->string('email', 255);
            $table->string('image', 255);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('site_topo_teams');
    }

}
