<?php namespace Site\Topo;

use System\Classes\PluginBase;

/**
 * Topo Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Site Portal',
            'description' => 'site da portal agencia',
            'author'      => 'Breno Douglas',
            'icon'        => 'icon-leaf'
        ];
    }

    public function registerComponents()
    {
        return [
            '\Site\Topo\Components\About' => 'about',
            '\Site\Topo\Components\Banner' => 'banner',
            '\Site\Topo\Components\Contact' => 'contact',
            '\Site\Topo\Components\Portifolio' => 'portifolio',
            '\Site\Topo\Components\Services' => 'services',
            '\Site\Topo\Components\Skills' => 'skills',
            '\Site\Topo\Components\Team' => 'team',
            '\Site\Topo\Components\Skills' => 'skills',
            '\Site\Topo\Components\Project' => 'project'
        ];
    }

    public function registerPermissions()
    {
        return [
            'site.topo.banner'         => ['tab' => 'Site', 'label' => 'Banner'],
            'site.topo.skill'          =>  ['tab' => 'Site', 'label' => 'Skills'],
            'site.topo.service'        => ['tab' => 'Site', 'label' => 'Serviços'],
            'site.topo.team'         => ['tab' => 'Site', 'label' => 'Equipe'],
            'site.topo.portifolio'         => ['tab' => 'Site', 'label' => 'Portifolio']
        ];
    }

    public function registerNavigation()
    {
        return [
            'site' => [
                'label'       => 'Site',
                'url'         => \Backend::url('site/topo/portifolio'),
                'icon'        => 'icon-copy',
                'permissions' => ['site.topo.portifolio'],
                'order'       => 500,
                'sideMenu' => [
                    'team' => [
                        'label'       => 'Equipe',
                        'url'         => \Backend::url('site/topo/team'),
                        'icon'        => 'icon-cog',
                        'permissions' => ['site.topo.team'],
                    ],
                    'portifolio' => [
                        'label'       => 'Portifolio',
                        'icon'        => 'icon-copy',
                        'url'         => \Backend::url('site/topo/portifolio'),
                        'permissions' => ['site.topo.portifolio']
                    ],
                    'banner' => [
                        'label'       => 'Banner',
                        'url'         => \Backend::url('site/topo/banner'),
                        'icon'        => 'icon-pencil',
                        'permissions' => ['site.topo.banner'],
                        'order'       => 500,
                    ],
                    'service' => [
                        'label'       => 'Serviços',
                        'url'         => \Backend::url('site/topo/service'),
                        'icon'        => 'icon-cog',
                        'permissions' => ['site.topo.service'],
                        'order'       => 500,
                    ],
                    'skill' => [
                        'label'       => 'Skills',
                        'url'         => \Backend::url('site/topo/skill'),
                        'icon'        => 'icon-bars',
                        'permissions' => ['site.topo.skill'],
                        'order'       => 500,
                    ],
                    'categories' => [
                        'label'       => 'Categoria Portifolio',
                        'icon'        => 'icon-list-ul',
                        'url'         => \Backend::url('site/topo/portifoliocategory'),
                        'permissions' => ['site.topo.portifolio']
                    ]
                ]
            ]
        ];
    }

}
