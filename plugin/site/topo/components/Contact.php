<?php namespace Site\Topo\Components;

use Cms\Classes\ComponentBase;

use Mail;

class Contact extends ComponentBase
{

    public $send;

    public function componentDetails()
    {
        return [
            'name'        => 'contact Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun() 
    {
        $this->send = false;
    }

    public function onSendEmail() 
    {
       
        $this->page['send'] = true;

        /*Mail::send('emails.reminder', ['user' => $user], function ($m) use ($user) {
            $m->to($user->email, $user->name)->subject('Your Reminder!');
        });*/
    }

}