<?php namespace Site\Topo\Components;

use Cms\Classes\ComponentBase;
use Site\Topo\Models\Skill as SkillModel;

class Skills extends ComponentBase
{

    public $skills;
    
    public function componentDetails()
    {
        return [
            'name'        => 'skills Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }


    public function onRun()
    {
        $this->skills = SkillModel::all();
    }
}