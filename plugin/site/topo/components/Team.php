<?php namespace Site\Topo\Components;

use Cms\Classes\ComponentBase;
use Site\Topo\Models\Team as TeamModel;

class Team extends ComponentBase
{

    public $members;

    public function componentDetails()
    {
        return [
            'name'        => 'team Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->members = TeamModel::all();
    }
}