<?php namespace Site\Topo\Components;

use Cms\Classes\ComponentBase;
use Site\Topo\Models\Service as ServiceModel;

class About extends ComponentBase
{

    public $services;

    public function componentDetails()
    {
        return [
            'name'        => 'about Component',
            'description' => 'Componente de sobre com serviços'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->services = ServiceModel::all();
    }
}