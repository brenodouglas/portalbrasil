<?php namespace Site\Topo\Components;

use Cms\Classes\ComponentBase;
use Site\Topo\Models\PortifolioCategory;
use Site\Topo\Models\Portifolio as PortifolioModel;

class Portifolio extends ComponentBase
{
    public $portifolios;

    public $categories;

    private $itemPerPage;

    public function componentDetails()
    {
        return [
            'name'        => 'portifolio Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->itemPerPage = 9;
        
        $this->categories = PortifolioCategory::all();
        $this->portifolios = PortifolioModel::limit($this->itemPerPage)->get();

        $this->page['more'] = PortifolioModel::count() > $this->itemPerPage;
        $this->page['total'] = 1;
    }

    public function onMoreItens()
    {
        $qtd = post('total');
        $total = $qtd + 9;

        $this->page['portifolios'] = PortifolioModel::limit($total)->get();
        $this->page['more'] = PortifolioModel::count() > $total;
        $this->page['total'] = $total;
    }
    
}