<?php namespace Site\Topo\Components;

use Cms\Classes\ComponentBase;
use Site\Topo\Models\Banner as BannerModel;

class Banner extends ComponentBase
{

    public $banners;

    public function componentDetails()
    {
        return [
            'name'        => 'banner Component',
            'description' => 'Banner topo'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->banners = BannerModel::all();
    }
    
}