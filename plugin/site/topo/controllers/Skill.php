<?php namespace Site\Topo\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Skill Back-end Controller
 */
class Skill extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Site.Topo', 'site', 'skill');
    }

    public function index()
    {
        $this->asExtension('ListController')->index();
    }

    public function create()
    {
        // $this->bodyClass = 'compact-container';
        // $this->addCss('/plugins/rainlab/blog/assets/css/rainlab.blog-preview.css');
        // $this->addJs('/plugins/rainlab/blog/assets/js/post-form.js');
        return $this->asExtension('FormController')->create();
    }

    public function update($recordId = null)
    {
        //$this->bodyClass = 'compact-container';
        // $this->addCss('/plugins/rainlab/blog/assets/css/rainlab.blog-preview.css');
        // $this->addJs('/plugins/rainlab/blog/assets/js/post-form.js');

        return $this->asExtension('FormController')->update($recordId);
    }
}